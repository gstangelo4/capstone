﻿/*
Greg St. Angelo
2.3.2018

Roman Numeral Converter
*/
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RomanNumeralConverter
{

    public partial class RomanNumeralConverter : Form
    {
        // numerals that are accepted
        private List<char> Numerals = new List<char>
        {
            'I',
            'V',
            'X',
            'L',
            'C',
            'D',
            'M'
        };

        /// <summary>
        /// Constructor
        /// </summary>
        public RomanNumeralConverter()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Click event for Convert.
        /// </summary>
        /// <param name="aSender"></param>
        /// <param name="e"></param>
        private void button_Convert_Click(object aSender, EventArgs e)
        {
            string _num = textBox_Number.Text.ToUpper();

            try
            {
                // verify only correct numerals were entered
                foreach(char c in _num)
                {
                    if(!Numerals.Contains(c))
                    {
                        throw new FormatException("Only accepts roman numerals I-M.");
                    }
                }

                textBox_Converted.Text = RConverter(_num);
                textBox_Number.Focus();
            }
            catch (FormatException fe)
            {
                MessageBox.Show(fe.Message, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                button_Reset.PerformClick();
            }
        }

        /// <summary>
        /// Click event for Reset.
        /// </summary>
        /// <param name="aSender"></param>
        /// <param name="e"></param>
        private void button_Reset_Click(object aSender, EventArgs e)
        {
            textBox_Number.Text = "";
            textBox_Converted.Text = "";

            textBox_Number.Focus();
        }

        /// <summary>
        /// Click event for Exit.
        /// </summary>
        /// <param name="aSender"></param>
        /// <param name="e"></param>
        private void button_Exit_Click(object aSender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Roman numeral conversion method.
        /// </summary>
        /// <param name="aNum">Roman numeral to convert to base 10 number.</param>
        /// <returns>string representation of the base 10 number.</returns>
        private string RConverter(string aNum)
        {
            int _num = 0;

            for (int i = 0; i < aNum.Length; i++)
            {
                switch(aNum[i])
                {
                    case 'I':
                        _num += 1;
                        break;
                    case 'V':
                        if(i != 0)
                        {
                            if(aNum[i-1] == 'I')
                            {
                                _num += 3;
                                break;
                            }
                        }
                        _num += 5;
                        break;
                    case 'X':
                        if (i != 0)
                        {
                            if (aNum[i - 1] == 'I')
                            {
                                _num += 8;
                                break;
                            }
                        }
                        _num += 10;
                        break;
                    case 'L':
                        if (i != 0)
                        {
                            if (aNum[i - 1] == 'X')
                            {
                                _num += 30;
                                break;
                            }
                        }
                        _num += 50;
                        break;
                    case 'C':
                        if (i != 0)
                        {
                            if (aNum[i - 1] == 'X')
                            {
                                _num += 80;
                                break;
                            }
                        }
                        _num += 100;
                        break;
                    case 'D':
                        if (i != 0)
                        {
                            if (aNum[i - 1] == 'C')
                            {
                                _num += 300;
                                break;
                            }
                        }
                        _num += 500;
                        break;
                    case 'M':
                        if (i != 0)
                        {
                            if (aNum[i - 1] == 'C')
                            {
                                _num += 800;
                                break;
                            }
                        }
                        _num += 1000;
                        break;
                }
            }
            return _num.ToString();
        }
    }
}
